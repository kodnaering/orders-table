FROM node:8.7.0

WORKDIR /app
ADD . .
RUN npm install
EXPOSE 4200

CMD npm run start
