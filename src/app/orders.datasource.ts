import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/exhaustMap';
import 'rxjs/add/operator/catch';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core'
import { MyPaginatorComponent } from './my-paginator.component';

export interface Order {
  customerId: string;
  dateTime: Date,
  orderId: number;
  paymentId: number;
  price: number;
  siteId: number;
  sku: string;
}

@Injectable()
export class OrderDataSource extends DataSource<any> {
	constructor (
    private http: HttpClient,
    private paginator: MyPaginatorComponent
  ) {
		super()
	}

	connect(): Observable<Order[]> {
		return Observable.merge(...[
			this.http.get('http://localhost:4300'),
			this.paginator.page,
		])
			.exhaustMap(() => {
				let page = this.paginator.pageIndex
				let size = this.paginator.pageSize
				return this.http.get(`http://localhost:4300?page=${page}&size=${size}`).catch(err => {
					this.paginator.goLeft()
					return []
				})
			})
  }

  disconnect() {}
}

