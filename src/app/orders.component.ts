import { Component, ViewChild } from '@angular/core';
import { OrderDataSource } from './orders.datasource';
import { MyPaginatorComponent } from './my-paginator.component';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'orders-data-table',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent {
	constructor (
		private http: HttpClient,
	) { }

  displayedColumns = [
		'customerId',
		'dateTime',
		'orderId',
		'paymentId',
		'price',
		'siteId',
		'sku',
	];
	dataSource: OrderDataSource | null;
  @ViewChild(MyPaginatorComponent) paginator: MyPaginatorComponent;

	ngOnInit () {
		this.dataSource = new OrderDataSource(this.http, this.paginator)
	}
}

