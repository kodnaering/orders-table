import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
  MatTableModule,
  MatButtonModule,
} from '@angular/material'
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { OrdersComponent } from './orders.component';
import { HttpClientModule } from '@angular/common/http';
import { OrderDataSource } from './orders.datasource';
import { MyPaginatorComponent } from './my-paginator.component';


@NgModule({
  declarations: [
    AppComponent,
    OrdersComponent,
    MyPaginatorComponent,
  ],
  imports: [
    BrowserModule,
		MatTableModule,
    MatButtonModule,
    HttpClientModule,
    NoopAnimationsModule,
  ],
  providers: [OrderDataSource],
  bootstrap: [AppComponent]
})
export class AppModule { }
