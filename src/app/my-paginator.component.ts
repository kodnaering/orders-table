import {
  Component,
  Input,
  Output,
  EventEmitter
} from '@angular/core';

@Component({
  selector: 'my-paginator',
  templateUrl: './my-paginator.component.html',
  styleUrls: ['./my-paginator.component.css']
})
export class MyPaginatorComponent {
  @Input() pageSize: number
  @Input() pageIndex: number
  @Output() page: EventEmitter<any> = new EventEmitter()

	ngOnInit () {
    if (!this.pageIndex) {
      this.pageIndex = 0
     }
	}

  goLeft() {
    if (this.pageIndex < 1) {
      return
    }
    this.pageIndex = this.pageIndex - 1
    this.page.emit(this.pageIndex)
  }

  goRight() {
    this.pageIndex = this.pageIndex + 1
    this.page.emit(this.pageIndex)
  }
}
